#include "AntaresESPHTTP.h"
#include <EEPROM.h>
//---Change ACCESSKEY---//
#define ACCESSKEY "1e8ca4656876fb2f:a919dc955359896e"
//------------------------------------

// ---Change SSID and Pasword---//
#define WIFISSID "GigaWiFi2"
#define PASSWORD "gigawifidds"
#define led D8
//------------------------------------

//---Global Variabel---
byte   size_Data  = 8;
const byte  banyak_Pin = 8;
unsigned long waktu,
              previousMillis = 0,
              intervalkirim=60000;
              
int     data_Pin[banyak_Pin]={D0,D1,D2,D3,D4,D5,D6,D7},
        data_button[banyak_Pin],
        data_eeprom[banyak_Pin],
        isi_eeprom[banyak_Pin],
        eeprom_address[banyak_Pin],
        counter_lastState=0,
        flag=0,
        counter_currentState=0;

String data_Value[banyak_Pin], 
       last_State[banyak_Pin],
       last_data[banyak_Pin]="1",
       data_Name[banyak_Pin] = {"1","2","3","4","5","6","7","8"},
       project_Name= "smartdevice",
       device_Name= "oee";
//-------------------------------------
Antares antares(ACCESSKEY);

void data_Read() {
   for(byte j = 0 ;j < banyak_Pin; j++) {
    data_button [j] = digitalRead(data_Pin[j]);
    if(data_button[j]==0) {
//      data_Value[j]="1";
      isi_eeprom[j]=1;
      EEPROM.put(eeprom_address[j],isi_eeprom[j]);
      EEPROM.commit();
    }
    if(data_button[j]==1) {
//      data_Value[j]="0";  
    }
   }
  Serial.print("Data EEPROM: ");
   baca_eeprom();
   Serial.println();   
}

void connect_WiFi()
{
    WiFi.begin(WIFISSID,PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
      pinMode(led,OUTPUT);
      digitalWrite(led,LOW);
      delay(200);
      Serial.println("Connecting.");
      digitalWrite(led,HIGH);
      delay(200);
      data_Read();
      Serial.print("Flag="); 
      Serial.println(flag);
    }
    antares.setDebug(false);
   // WiFi.persistent( false );
    antares.wifiConnection(WIFISSID,PASSWORD); 
}

void setup() {
    Serial.begin(115200);
    pinMode(led, OUTPUT);
    antares.setDebug(true);
    antares.wifiConnection(WIFISSID,PASSWORD);
    for(byte i=0; i< banyak_Pin;i++){
        pinMode(data_Pin[i],INPUT);
    }
}


void dataread_online(){
  unsigned long currentMillis = millis();
  Serial.print("Data :");
   for(byte m = 0 ;m < banyak_Pin; m++) {
    data_button [m] = digitalRead(data_Pin[m]);
    if(data_button[m]==0) {
      data_Value[m]="1";
    }
    if(data_button[m]==1) {
      data_Value[m]="0";  
    }
    Serial.print(data_button[m]);
   }
   Serial.println();
//   for(byte l=0;l<banyak_Pin;l++){
//              data_Value[l]=last_data[l];
//          }
//
//    if(currentMillis - previousMillis >=intervalkirim){
//      previousMillis=currentMillis;
//      kirim_antares();
//    }
}
void hapus_eeprom(){
  for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.put(eeprom_address[i], 0);
  }
//  EEPROM.end();
  delay(1000);
  Serial.println("EEPROM erased");
}
void baca_eeprom(){
  for(byte l=0; l< banyak_Pin; l++){
      data_eeprom[l]=EEPROM.get(eeprom_address[l],isi_eeprom[l]);
      Serial.print(data_eeprom[l]);
    }
}
void kirim_antares(){
  Serial.println(antares.storeData(
                project_Name, 
                device_Name, 
                data_Name, 
                data_Value,
                banyak_Pin
                )           
              );
}
void kirim_Data_Cloud() {
  unsigned long currentMillis = millis();
     counter_currentState=0;
     counter_lastState=0;
    for(size_Data = 0;size_Data < banyak_Pin;size_Data++) { 
          if(data_Value[size_Data]== "1")counter_currentState++;
          if(last_State[size_Data]== "1")counter_lastState++;
          if(data_Value[size_Data] != last_State[size_Data])last_State [size_Data] = data_Value [size_Data];
        }
    if(currentMillis - previousMillis >=intervalkirim){
      previousMillis=currentMillis;
      kirim_antares();
    }   
    if((counter_currentState - counter_lastState == 1) || (counter_currentState - counter_lastState == -1)  ) { 
          for(byte k=0;k<banyak_Pin;k++)
          {
            last_data[k]=data_Value[k];
            if(data_Value[k]=="0") data_Value[k]="0";
          }
          kirim_antares();
          for(byte l=0;l<banyak_Pin;l++){
              data_Value[l]=last_data[l];
          }         
          Serial.println(counter_currentState);
          Serial.println(counter_lastState);    
        }     
}

void loop() {
  if(WiFi.status()!=WL_CONNECTED){
    connect_WiFi();
    flag=0;
    Serial.println("Flag: ");
    Serial.print(flag);
  }
  if((WiFi.status()==WL_CONNECTED)&&flag==0){
    flag=1;
    for(byte a=0; a< banyak_Pin; a++){
      data_eeprom[a]=EEPROM.get(eeprom_address[a],isi_eeprom[a]);
      data_Value[a]=String(data_eeprom[a]);
    }
    kirim_antares();
    delay(500);
    hapus_eeprom();
    Serial.println("Flag: ");
    Serial.print(flag);
  }
  else{
    dataread_online();
    kirim_Data_Cloud();
    //delay(500);
  }
  Serial.print("Flag: ");
  Serial.print(flag); 
}
