//#include <SoftwareSerial.h>
//SoftwareSerial sw(16,17);  //RX TX
//#include <HardwareSerial.h>
//HardwareSerial Serial2(2);


const int quantity = 34,
          quality = 35,
          Qn = 2,
          Ql = 13;      

int quantityCounter = 0,  
    quantityState = 0,
    lastQuantityState = 0;
    
int qualityCounter = 0,  
    qualityState = 0,
    lastQualityState = 0; 
String qtt,qlt;
char data_machine[100];
unsigned long waktu,
              currentMillis,
              previousMillis = 0,
              intervalkirim=60000;

void kirim_data(){
   Serial.println("kirim ke modul koneksi!");  
   Serial.print("Quantity:");
   Serial.print(quantityCounter);
   qtt=String(quantityCounter);
   Serial.print(" pcs ");
   Serial.print("Quality:");
   Serial.print(qualityCounter);
   Serial.print(" pcs \n");
   qlt=String(qualityCounter);
   Serial.println();
   Serial2.print("Qtt:"+qtt+" Qlt:"+qlt );
//   Serial2.print(qtt);
//   Serial2.print(" Qlt:");
//   Serial2.print(qlt);
//   Serial2.write("send Quality:"+qtt+" Quality:"+qlt);  
     
}

void setup() {
  pinMode(quantity, INPUT);
  pinMode(quality, INPUT);
  pinMode(Qn, OUTPUT);
  pinMode(Ql, OUTPUT);
  Serial.begin(115200);
//  Serial2.begin(115200);
  Serial2.begin(115200);
}

void Qtt(){
  quantityState = digitalRead(quantity);
  if (quantityState != lastQuantityState) {
    // if the state has changed, increment the counter
    if (quantityState == HIGH) {
      // if the current state is HIGH then the button went from off to on:
      quantityCounter++;
      Serial.print("Quantity: ");
      Serial.print(quantityCounter);
      Serial.print(" pcs \n");
    } 
     delay(10);
  }
  lastQuantityState = quantityState; 
  if (quantityState == 0) {
    digitalWrite(Qn, HIGH);
  } else {
    digitalWrite(Qn, LOW);
  }
}

void Qlt(){
  qualityState = digitalRead(quality);
  if (qualityState != lastQualityState) {
    // if the state has changed, increment the counter
    if (qualityState == HIGH) {
      // if the current state is HIGH then the button went from off to on:
      qualityCounter++;
      Serial.print("Quality: ");
      Serial.print(qualityCounter);
      Serial.print(" pcs \n");
    } 
     delay(10);
  }
  lastQualityState = qualityState; 
  if (qualityState == 0) {
    digitalWrite(Ql, HIGH);
  } else {
    digitalWrite(Ql, LOW);
  }
}

void loop() {
  currentMillis=millis();
  Qtt();
  Qlt();
  if(currentMillis - previousMillis >=intervalkirim){
      previousMillis=currentMillis;
      kirim_data();
  }
  delay(10);
}
